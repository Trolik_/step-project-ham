'use strict';
const hiddenImg = document.querySelectorAll('.grid > .invisible-masonry');
const masonryAddMoreBtn = document.querySelector('.masonry-btn');
const galleryLoader = document.querySelector('.dynamic-loader-masonry');

const masonryGallary = document.querySelector('.grid');

let masonry = new Masonry(masonryGallary, {
	itemSelector: ".grid-item",
	gutter: 18,
});

masonryAddMoreBtn.addEventListener('click', (e) => {
	e.preventDefault();
	galleryLoader.style.opacity = 100;

	setTimeout(() => {
		galleryLoader.style.opacity = 0;

		for (let i = 0; i < hiddenImg.length; i++) {
			hiddenImg[i].classList.remove('invisible-masonry')
		}
		let masonry = new Masonry(masonryGallary, {
			itemSelector: ".grid-item",
			gutter: 18,
		})
		masonryAddMoreBtn.classList.add('invisible-masonry');
	}, 1500)
})

