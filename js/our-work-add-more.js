'use strict';

const tabsOfOurWorks = document.querySelector('.our-works-tabs');
const allCards = document.querySelectorAll('.work-card');
const loadMoreButton = document.querySelector('.our-works-btn');
const loader = document.querySelector('.dynamic-loader');

tabsOfOurWorks.addEventListener('click', (e) => {
	allCards.forEach(element => element.classList.add('hide-card'))

	if (e.target !== e.currentTarget) {
		document.querySelector('.our-works-active').classList.remove('our-works-active');
		e.target.classList.add('our-works-active');

		const data = e.target.innerText === 'All'
			? document.querySelectorAll('.work-card')
			: document.querySelectorAll(`[data-tab-category="${e.target.innerText.toLowerCase()}"]`);

		if (data.length > 12) {
			loadMoreButton.classList.remove('invisible');
			for (let i = 0; i < 12; i++) allCards[i].classList.remove('hide-card');
		} else {
			loadMoreButton.classList.add('invisible');
			data.forEach(element => element.classList.remove('hide-card'))
		}
	}
})

if (allCards.length > 12) {
	loadMoreButton.classList.remove('invisible');
}

loadMoreButton.addEventListener('click', (e) => {
	e.preventDefault();
	loader.style.opacity = 100;

	setTimeout(() => {
		loader.style.opacity = 0;

		const invisibleCards = document.querySelectorAll('.hide-card');
		if(allCards.length > invisibleCards.length) {
			for (let i = 0; i < 12; i++) {
				invisibleCards[i].classList.remove('hide-card');
			}
		}
		if(!(document.querySelector('.hide-card'))) {
			loadMoreButton.classList.add('invisible');
		}
	}, 1500)
})

