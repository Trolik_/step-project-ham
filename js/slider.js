'use strict';

const sliderPrevBtn = document.querySelector('.previous-btn');
const sliderNextBtn = document.querySelector('.next-btn');

let prevSelectedUser = document.querySelector('.description-active');
let selectedUser = Number(prevSelectedUser.dataset.slider);
let selectedUserImg = document.querySelector('.selected');
let selectedScaledImg = Number(selectedUserImg.dataset.scale);

function elementsChanger() {
	prevSelectedUser = document.querySelector(`[data-slider="${selectedUser}"]`);
	prevSelectedUser.classList.add('description-active');

	selectedUserImg = document.querySelector(`[data-scale="${selectedScaledImg}"]`);
	selectedUserImg.classList.add('selected');
}

sliderPrevBtn.addEventListener('click', (e) => {
	e.preventDefault();
	prevSelectedUser.classList.remove('description-active');
	selectedUserImg.classList.remove('selected');

	if (selectedUser === 1) {
		selectedUser = 4;
		selectedScaledImg = 4;
		elementsChanger();
	} else {
		selectedUser--;
		selectedScaledImg--;
		elementsChanger();
	}
})

sliderNextBtn.addEventListener('click', () => {
	prevSelectedUser.classList.remove('description-active');
	selectedUserImg.classList.remove('selected');

	if (selectedUser === 4) {
		selectedUser = 1;
		selectedScaledImg = 1;
		elementsChanger();
	} else {
		selectedUser++;
		selectedScaledImg++;
		elementsChanger();
	}
})

const allInfoAboutPeople = document.querySelector('.people-description-slider');

allInfoAboutPeople.addEventListener('click', (e) => {
	if (e.target !== e.currentTarget && e.target.classList.contains('user-photo')) {
		if (!e.target.classList.contains('selected')) {
			prevSelectedUser.classList.remove('description-active');
			selectedUserImg.classList.remove('selected');

			selectedUser = Number(e.target.dataset.scale);
			selectedScaledImg = selectedUser;
			elementsChanger();
		}
	}
})