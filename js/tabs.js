const tabs = document.querySelector(".our-services-list");
tabs.addEventListener("click", (e) => {

	document.querySelector(".our-services-item.active").classList.remove("active");
	e.target.classList.add("active");

	const data = e.target.getAttribute("data-tab-content");

	document.querySelector(".our-services-tab.active").classList.remove("active");
	document.querySelector(`.our-services-tabs-content [data-tab-content=${data}]`).classList.add("active");
});
